
package ar.com.nmv.dao;

import ar.com.nmv.domain.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IPersonaDao extends JpaRepository<Persona, Long> {
    
}
