package ar.com.nmv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PracticaSgcApplication {

	public static void main(String[] args) {
		SpringApplication.run(PracticaSgcApplication.class, args);
	}

}
