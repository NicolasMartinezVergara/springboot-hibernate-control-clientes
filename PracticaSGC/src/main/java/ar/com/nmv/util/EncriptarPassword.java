
package ar.com.nmv.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;


public class EncriptarPassword {
    
    public static void main(String[] args) {
        String password = "123";
        String password2 = "456";
        System.out.println("password 1 encriptado: " + encriptarPassword(password));
        System.out.println("password 2 encriptado: " + encriptarPassword(password2));
                
    }
    
    public static String encriptarPassword(String password){
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.encode(password);
    }
    
}
