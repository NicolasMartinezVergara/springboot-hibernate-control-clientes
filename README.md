Proyecto del curso “Universidad Spring 2021 – Spring Framework y Spring Boot” (UDEMY)
Sistema de control de clientes utilizando Spring Boot.con capa de datos, capa de negocio y capa web con patrón de diseño MVC.
Internacionalización (cambio de idiomas, archivo de propiedades), y manejo de usuarios con roles, conectado a base de datos MySQL.

El objetivo de este ejercicio fue realizar una aplicación java para gestionar una lista de clientes de un negocio X, con su nombre, apellido,
saldo adeudado. Además también tener un control del número total de clientes y el saldo total de los deudores.
La idea principal fue trabajar con el framework de SpringBoot, y así analizar las ventajas que se tiene a nivel código, tanto para interactuar
con la base de datos de manera más simple, como para exponer información al cliente.
Más también aplicar el concepto de transacciones en la capa de negocio.

Desarrollado con Apache Netbeans, utilizando pluggin de SpringFramework (similar a SpringInitializer), versión 2.6.1 .
Administrador de librerías Maven.
Base de datos utilizada MySQL 8 con MySQL Workbench.
Capa de datos utilizando JPA/Hibernate (spring-boot-starter-data-jpa).
Capa web con SpringBoot Starter Web y presentación con tecnología Thymeleaf y Bootstrap para los estilos.
Todas estas dependencias del proyecto descargadas mediante la configuración del archivo pom.xml. 
Servidor de aplicaciones Tomcat.

Las clases de dominio o modelo están definidas como @Entity para que JPA pueda relacionarlas con los registros de la base de datos.
Cada objeto tiene su atributo id (o llave primaria de acceso), que será creada de manera automática y autoincrementable desde la base de datos 
por cada registro creado (de ahí la anotación @GeneratedValue en cada una de las clases de dominio).
Al escribir la anotación @Data sobre la clase, automáticamete se generan los métodos "get" y "set" de cada atributo, más el método toString,
equals y hashtag, para que sea considerada como un java bean (anotación importada de la librería de lombok).

En la ruta springboot-hibernate-control-clientes/PracticaSGC/src/main/resources, se encuentra el archivo de propiedades"application.properties".
En este archivo se configuran las propiedades y sus valores para la conexión a la base de datos por medio de JDBC y Mysql Driver.
Así como también la configuración del log para que muestre por consola los SQL que se ejecuten en las transacciones y saber si el commit 
de los datos se realizó con éxito.
MySQL en localhost:3306 .

En la capa de datos, dentro de la ruta springboot-hibernate-control-clientes/PracticaSGC/src/main/java/ar/com/nmv/dao, se encuentran todas 
las clases DAO de cada entidad.
En este caso sólo se define la interface de PersonaDAO, que extienden de la interface JpaRepository, y ya de esta manera puede inyectarse una 
instancia de esta interface para luego llamar a sus métodos desde la capa de negocio (que ya tiene por default los métodos "findAll, 
findById, save, delete", entre otros).
Directamente se importarán los objetos completos de tipo Persona desde MySQL.

Siguiendo con la capa de negocio en la ruta springboot-hibernate-control-clientes/PracticaSGC/src/main/java/ar/com/nmv/service, se encuentra
la interface de PersonaService más su implementación (respetando el concepto de alta cohesión y bajo acoplamiento entre componentes).
En la interface se define la firma de los métodos.
En la clase de implementación, se inyecta la dependencia de PersonaDao mediante la anotación de @Autowired para así acceder a sus métodos.
Y sobre cada método de esta clase de servicio se escribe la anotación @Transactional para el manejo de transacciones, iniciando la transacción
de manera automática para realizar un commit en el caso de éxito o un rollback en el caso de fallo (es decir, que dentro de la transacción, 
si falla una de las operaciones, ninguna de estas operaciones será guardada en la basede datos).
Puede también configurarse como "readOnly" en el caso que sólo haya que leer información y no abrir una transacción para persistir un dato.
A esta clase de PersonaService se le agrega la anotación @Service, para que de esta manera pueda ser inyectada en los servlets o controladores
de la capa web.

La capa Web está desarrollada siguiendo el patrón de diseño MVC (Model-View-Controller).
En la ruta springboot-hibernate-control-clientes/PracticaSGC/src/main/java/ar/com/nmv/web, está el servlet controlador que recibe las 
peticiones del cliente por medio de las vistas (HTML´s), y a su vez se conecta con la capa de negocio y con las clases de modelo.
A esta clase se le antepone la anotación de @Controller, para el manejo de las peticiones (request), las respuestas (response).
Se inyecta primero la interface de PersonaService con la anotación @Autowired, y así se accede a los métodos de la
implementación de dicha interface, tanto para agregar registros, modificarlos, eliminarlos, recuperar datos y redireccionar entre las 
diferentes vistas (en algunos casos, realizando un redirect con la petición y la respuesta del cliente).
Cada método tiene su anotación, en base a si son peticiones de tipo get (@GetMapping) o post (@PostMapping), con sus respectivos paths para
acceder desde las vistas.


En la carpeta springboot-hibernate-control-clientes/PracticaSGC/src/main/resources se encuentran también los archivos de propiedades, que serán leídos desde las vistas para el manejo de 
los textos que se visualicen (como títulos, etiquetas, texto en botones, etc.). Con esto se obtiene la ventaja de manejar el concepto de
internacionalización, para que se muestre la página en el idioma que el cliente desee (sea por la configuración por default de su navegador,
o por medio de los links al pie de la página).
En este caso, se trabajó sólo con idioma español e inglés (es - en, con sus respectivos archivos de propiedades).
Y dentro de esta ruta, en la carpeta de templates, están las vistas, archivos HTML aplicando Thymeleaf con el prefijo "th" para manejar 
sus tags.
Con Thymeleaf se utiliza, entre otros, el concepto de "layout" para poder incrustar plantillas dentro de otras para 
trabajar por separado con cada parte de la página y obtener así una mejor organización.

Finalmente, en la ruta springboot-hibernate-control-clientes/PracticaSGC/src/main/java/ar/com/nmv/web, se encuentran también la clases
SecurityConfig y WebConfig, ambas clases definidas con la anotación @Configuration.
La clase SecurityConfig extiende de WebSecurityConfigurerAdapter (de SpringBoot security), y aquí se definen los métodos para la autenticación
y autorización del usuario para poder acceder a la aplicación o a determinadas rutas (dependiendo del rol, USER o ADMIN, tendrá autorización
para realizar determinadas acciones). Cada rol tiene su contraseña encriptada.
La clase WebConfig implementa la interface WebMvcConfigurer, y aquí están los métodos para manejar el concepto de internacionalización,
mediante los métodos relacionados con el objeto LocaleResolver y los interceptores.




